# myTest

#### Description
use ssm+maven，Carry out simple add, delete, change check operation.

Check out the blog for details：[click here](https://blog.csdn.net/qq_40558766/article/details/108030251)

GitHub address：[click here](https://github.com/bbSpider/myTest)

#### Software Architecture
Software Architecture Description
![输入图片说明](https://images.gitee.com/uploads/images/2020/0902/173133_698342ca_8001859.jpeg "1599033399(1).jpg")


#### Project conditions

Frame：spring+springMVC+Mybatis

Project management tool：Maven

Code writing tool：IntelliJ IDEA 2020.1 x64

Database：MySQL

Database tool：Navicat Premium


No one's supposed to see this. I'm a pig