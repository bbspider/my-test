# myTest

#### 介绍
使用ssm+maven，进行简单的增删改查操作

详细可查看博客：[点击这里](https://blog.csdn.net/qq_40558766/article/details/108030251)

GitHub地址：[点击这里](https://github.com/bbSpider/myTest)

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0902/173133_698342ca_8001859.jpeg "1599033399(1).jpg")


#### 项目条件

框架：spring+springMVC+Mybatis

项目管理工具：Maven

代码编写工具：IntelliJ IDEA 2020.1 x64

数据库：MySQL

数据库工具：Navicat Premium

